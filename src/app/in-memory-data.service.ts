import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { User } from './user.model';


@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService {

  constructor() { }


  createDb() {
    let users:User[] = [
        {
          id: 1,
          firstName: 'Abbie',
          lastName: 'Andrews',
          address: '8851 Upper St. Teaneck, NJ 07666',
          mail: 'abbie@mail.com',
          phone:'(120) 340-5630'
      },
      {
          id: 2,
          firstName: 'Syeda',
          lastName: 'Ferguson',
          address: '7411 Ryan St. Fort Lauderdale, FL 33308',
          mail: 'syeda@mail.com',
          phone:'(120) 340-5630'
      },
      {
          id: 3,
          firstName: 'Fern',
          lastName: 'Robertson',
          address: '8345 Court St. Saint Paul, MN 55104',
          mail: 'fern@mail.com',
          phone:'(120) 340-5630'
      },
      {
          id: 4,
          firstName: 'Gabriella',
          lastName: 'Johnston',
          address: '9385 Academy St. De Pere, WI 54115',
          mail: 'gabriella@mail.com',
          phone:'(120) 340-5630'
      },
      {
          id: 5,
          firstName: 'Troy',
          lastName: 'Chambers',
          address: '153 Bay View Street Fayetteville, NC 28303',
          mail: 'troy@mail.com',
          phone:'(120) 340-5630'
      },
      {
          id: 6,
          firstName: 'Ana',
          lastName: 'Hamilton',
          address: '531 Parkside St. Riverview, FL 33569',
          mail: 'ana@mail.com',
          phone:'(120) 340-5630'
      },
      {
          id: 7,
          firstName: 'Khadija',
          lastName: 'Moore',
          address: '105 Seaview Street Oak Lawn, IL 60453',
          mail: 'khadija@mail.com',
          phone:'(120) 340-5630'
      },
      {
          id: 8,
          firstName: 'Amelia',
          lastName: 'Lowe',
          address: '564 Champion St. La Vergne, TN 37086',
          mail: 'amelia@mail.com',
          phone:'(120) 340-5630'
      },
      {
          id: 9,
          firstName: 'Victoria',
          lastName: 'Reid',
          address: '40 Depot St. Milford, MA 01757',
          mail: 'victoria@mail.com',
          phone:'(120) 340-5630'
      },
      {
          id: 10,
          firstName: 'Lauren',
          lastName: 'Fleming',
          address: '9153 Bayport Drive Philadelphia, PA 19111',
          mail: 'lauren@mail.com',
          phone:'(120) 340-5630'
      }
    ];
    return {users};
  }


}
