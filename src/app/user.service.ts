import { Injectable } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { User } from './user.model';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})


export class UserService {
 
  private url = "api/users";
  
  constructor(private http:HttpClient) { }

  
  getUsers():Observable<User[]>{
      return this.http.get<User[]>(this.url).pipe(catchError(error => throwError(error)));
  }


  postUser(user: User): Observable<User> {
    return this.http.post<User>(this.url, user, httpOptions).pipe(catchError(error => throwError(error)));
  }
 

  getSearch(str:string){
    return this.http.get<User[]>(`${this.url}/?firstName=${str}`).pipe(catchError(error => throwError(error)));
  }
  


  



}
