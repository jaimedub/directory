import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users = [];

  user:User = {
    id:null,
    firstName:"",
    lastName:"",
    address:"",
    mail:"",
    phone:""
  }

  errorMsg:string;
 
  
  form: FormGroup;
  buah: FormGroup;

  searchStr:string;



  constructor(private userService:UserService) { }

  ngOnInit() {
    this.getUsers();
  } 


  openModal(){
    $(".modal").modal();
  }


  getUsers(){
    $(".loader").fadeIn();
    this.userService.getUsers().subscribe(
        data => {
          this.users = data;
          $(".loader").fadeOut();
        },
        error => {
          this.users = [];
          $(".loader").fadeOut();
        }
      );
  }


  postUser(){
    $(".loader").fadeIn();
    $(".save").prop('disabled', true);
    this.userService.postUser(this.user).subscribe(
      data => {
        console.log(data);
        this.users.push(data);
        $(".modal").modal("close");
        $(".loader").fadeOut();
      },
      error => {
        console.log(error);
        this.errorMsg = "Server error please try again later";
        $(".loader").fadeOut();
        $(".save").prop('disabled', false);
      }
    );
  }



  getSearch(){
    let search = this.users.filter(
      str => str.firstName === this.searchStr || str.lastName === this.searchStr || str.address === this.searchStr || str.mail === this.searchStr || str.phone === this.searchStr
    );

    if(search.length != 0){
      this.users = search;
    }

    console.log(search);
  }


}
